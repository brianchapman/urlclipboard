package main

import (
	"github.com/atotto/clipboard"
	"log"
	"os/exec"
	"strings"
	"time"
)

func openInSafari(theURL string) {
	cmd := "/usr/bin/open" // Path on High Sierra, Yosemite too
	cmdArgs := []string{"-a", "Safari", theURL}
	err := exec.Command(cmd, cmdArgs...).Run()
	if err != nil {
		log.Println("ERROR opening URL - ", err)
	}
}

func searchDomainWhitelist(clipboardContents string) {

	if strings.Contains(clipboardContents, "washingtonpost.com") {
		openInSafari(clipboardContents)
	}

	if strings.Contains(clipboardContents, "nytimes.com") {
		openInSafari(clipboardContents)
	}

	if strings.Contains(clipboardContents, "bloomberg.com") {
		openInSafari(clipboardContents)
	}
	if strings.Contains(clipboardContents, "wsj.com") {
		openInSafari(clipboardContents)
	}
	if strings.Contains(clipboardContents, "bostonglobe.com") {
		openInSafari(clipboardContents)
	}
	if strings.Contains(clipboardContents, "theglobeandmail.com") {
		openInSafari(clipboardContents)
	}

}

func main() {
	var currentText string
	var lastText string

	currentText = "blankstring"
	lastText = "blankstring"
	howmany := 200
	for howmany > 1 {
		time.Sleep(100 * time.Millisecond)
		currentText, _ = clipboard.ReadAll()
		if currentText != lastText {
			if strings.Contains(currentText, "http") {
				searchDomainWhitelist(currentText)
			}
			lastText = currentText
		}
	}

}

